package main.android.interfaces;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;

import main.exceptions.checked.EmulatorException;

public interface AndroidEmulator {

	public void start() throws EmulatorException;

	public void stop() throws UnknownHostException, IOException;

	public boolean is_started();

	public void unlock_screen();

	public void install_apk(File apk);

	public void install_test_buildxm(File buildxml);

	public boolean is_installed(String apkPath);

	public void uninstall(String apkPath);

	public void run_test_instrumentation(String testName);
	
	public void  install_test_and_run_test_buildxml(File buildxml);

	public void pull_logs();

	public String get_name();

	public File get_root_folder();

	public int get_port();

	public String get_serial();

}
