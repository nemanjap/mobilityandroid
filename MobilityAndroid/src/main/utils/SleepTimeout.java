package main.utils;

public class SleepTimeout {

	public static final int SHORT = 2000;
	public static final int MEDIUM = 10000;
	public static final int LONG = 30000;
	public static final int EXTRA_LONG = 300000;
	
}
