package main.exceptions.unchecked;

public class MobilityException extends RuntimeException {

	private static final long serialVersionUID = 13579L;

	public MobilityException(String context, String problem, String solution) {
		super("\nContext: " + context + "\nProblem: " + problem
				+ "\nSolution: " + solution);
	}

	public MobilityException(Throwable t) {
		super(t);
	}

	public MobilityException(String context, String problem, String solution,
			Throwable t) {
		super("\nContext: " + context + "\nProblem: " + problem
				+ "\nSolution: " + solution, t);
	}

}
