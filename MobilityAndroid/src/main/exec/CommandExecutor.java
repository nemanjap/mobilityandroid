package main.exec;

import java.util.logging.Level;

import main.exceptions.unchecked.MobilityException;
import main.utils.Log;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.launcher.CommandLauncher;
import org.apache.commons.exec.launcher.CommandLauncherFactory;

public class CommandExecutor {

	public static String exec(CommandLine command) {
		return exec(command, 300000);
	}

	public static String exec(CommandLine command, long timeoutInMilliseconds) {

		LogCollectingOutputStream outputStream = new LogCollectingOutputStream();
		DefaultExecutor executor = new DefaultExecutor();

		executor.setWatchdog(new ExecuteWatchdog(timeoutInMilliseconds));
		PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
		executor.setStreamHandler(streamHandler);
		try {
			executor.execute(command);
		} catch (Exception e) {
			MobilityException mex = new MobilityException(
					"Executing the desired command",
					"An error occured while executing the desired shell command: "
							+ command,
					"Check if the command was entered correctly", e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}

		return (outputStream.getOutput());

	}

	public static Process exec_async(CommandLine command) {

		CommandLauncher launcher = CommandLauncherFactory.createVMLauncher();

		try {
			Process startedProcess = launcher.exec(command, null);
			return startedProcess;
		} catch (Exception e) {
			MobilityException mex = new MobilityException(
					"Executing the desired command",
					"An error occured while executing the desired shell command: "
							+ command,
					"Check if the command was entered correctly", e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}

	}

}
