package main.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcher {

	public static String get_data(String regex, String data, int group) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(data);
		if (matcher.find()) {
			return matcher.group(group);
		}
		return "";
	}

}
