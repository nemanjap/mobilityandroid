An application used for running a suite of tests on an Android emulator.

It sets up an environment with emulators, installs the desired Application Under Test and then runs a Test Suite specified by a build.xml configuration file.