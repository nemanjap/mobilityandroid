package main.utils;

import java.io.File;
import java.util.logging.Level;

import main.exceptions.unchecked.MobilityException;

public class Ant {

	public static final String ANT_HOME = "ANT_HOME";

	public static String home() {

		String antHome = System.getenv(ANT_HOME);

		if (antHome == null) {
			MobilityException mex = new MobilityException("Locating the "
					+ ANT_HOME + " environment variable",
					"Environment variable " + ANT_HOME
							+ " was not found on your system.", "Set the "
							+ ANT_HOME + " varible on your system.");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}
		return antHome;
	}

	private static File get_tool(String toolName) {
		StringBuffer tool = new StringBuffer();
		tool.append(home());
		tool.append(File.separator);
		tool.append("bin");
		tool.append(File.separator);

		File file = new File(tool.toString(), toolName);

		if (file == null || !file.exists() || !file.isFile()) {
			MobilityException mex = new MobilityException(
					"Locating the " + file.getAbsolutePath() + "file",
					"The file " + file.getAbsolutePath()
							+ " could not be found.",
					"Check if the "
							+ ANT_HOME
							+ " environment variabe is pointing to the right location.");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}

		return file;

	}
	
	public static File ant(){
		return get_tool("ant");
	}
	
}
