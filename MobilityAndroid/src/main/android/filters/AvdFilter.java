package main.android.filters;

import java.io.File;
import java.io.FileFilter;

public class AvdFilter implements FileFilter {

	public boolean accept(File pathName) {
	
		String fileName = pathName.getName();
		
		if(fileName.endsWith(".avd")){
			return true;
		}
		
		return false;
		
	}
}
