package main.utils;

public class OS {

	private static String OS = System.getProperty("os.name").toLowerCase();

	public static boolean is_windows() {
		return (OS.indexOf("win") >= 0);
	}

	public static String get_platform_executable_suffix_exe() {
		return is_windows() ? ".exe" : "";
	}

	public static String get_platform_executable_suffix_bat() {
		return is_windows() ? ".bat" : "";
	}

	public static boolean is_mac() {
		return (OS.indexOf("mac") >= 0);
	}

	public static boolean is_unix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS
				.indexOf("aix") > 0);
	}

	public static boolean is_solaris() {
		return (OS.indexOf("sunos") >= 0);
	}

	public static String get_OS() {
		if (is_windows())
			return "Windows";
		else if (is_unix())
			return "Unix";
		else if (is_mac())
			return "Mac";
		else if (is_solaris())
			return "Solaris";
		else
			return "Unidentified OS";
	}

}
