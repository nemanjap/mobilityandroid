package main.exceptions.checked;

public class EmulatorException extends Exception {

	private static final long serialVersionUID = 13579L;

	public EmulatorException(String context, String problem, String solution) {
		super("\nContext: " + context + "\nProblem: " + problem + "\nSolution: "
				+ solution);
	}

	public EmulatorException(Throwable t) {
		super(t);
	}

	public EmulatorException(String context, String problem, String solution,
			Throwable t) {
		super("\nContext: " + context + "\nProblem: " + problem + "\nSolution: "
				+ solution, t);
	}

}
