package main.utils;

import java.io.File;
import java.util.logging.Level;

import main.exceptions.unchecked.MobilityException;

public class Java {

	private static final String JAVA_HOME = "JAVA_HOME";

	public static String home() {

		String javaHome = System.getenv(JAVA_HOME);

		if (javaHome == null) {
			javaHome = System.getProperty("java.home");
		}

		if (javaHome == null) {
			MobilityException mex = new MobilityException(
					"Trying to locate the " + JAVA_HOME
							+ " environment variable", "Environment variable "
							+ JAVA_HOME + " was not found on your system.",
					"Set the " + JAVA_HOME
							+ " environment variable on your system.");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}

		return javaHome;
	}

	private static File get_tool(String toolName) {
		StringBuffer tool = new StringBuffer();
		tool.append(home());
		tool.append(File.separator);
		tool.append("bin");
		tool.append(File.separator);

		File file = new File(tool.toString(), toolName
				+ OS.get_platform_executable_suffix_exe());

		if (file == null || !file.exists() || !file.isFile()) {
			MobilityException mex = new MobilityException(
					"Locating the " + file.getAbsolutePath() + "file",
					"The file " + file.getAbsolutePath()
							+ " could not be found.",
					"Check if the "
							+ JAVA_HOME
							+ " environment variabe is pointing to the right location.");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}

		return file;

	}
}
