package main.utils;

import java.io.File;
import java.util.logging.Level;

import main.exceptions.unchecked.MobilityException;

public class AndroidSdk {

	public static final String ANDROID_HOME = "ANDROID_HOME";
	public static final String ANDROID_SDK_HOME = "ANDROID_SDK_HOME";
	public static final String avdParentFolder = ".android";
	public static final String avdFolder = "avd";

	public static String home() {

		String androidHome = System.getenv(ANDROID_HOME);

		if (androidHome == null) {
			MobilityException mex = new MobilityException("Locating the "
					+ ANDROID_HOME + " environment variable",
					"Environment variable " + ANDROID_HOME
							+ " was not found on your system.", "Set the "
							+ ANDROID_HOME + " varible on your system.");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}

		return androidHome;

	}

	public static File avd_home() {

		String androidSdkHome = System.getenv(ANDROID_SDK_HOME);

		if (androidSdkHome == null) {
			MobilityException mex = new MobilityException("Locating the "
					+ ANDROID_SDK_HOME + " environment variable",
					"Environment variable " + ANDROID_SDK_HOME
							+ " was not found on your system.", "Set the "
							+ ANDROID_SDK_HOME + " varible on your system.");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}

		File path = get_directory(androidSdkHome, avdParentFolder);

		return get_directory(path.toString(), avdFolder);

	}

	private static File get_directory(String directoryName) {
		return get_directory(null, directoryName);
	}

	private static File get_directory(String path, String directoryName) {

		if (path == null) {
			path = home();
		}

		StringBuffer directoryPath = new StringBuffer();
		directoryPath.append(path);
		directoryPath.append(File.separator);
		directoryPath.append(directoryName);
		directoryPath.append(File.separator);

		File dir = new File(directoryPath.toString());

		if (dir == null || !dir.exists() || !dir.isDirectory()) {
			MobilityException mex = new MobilityException(
					"Locating the specified directory.",
					dir.getAbsolutePath() + " could not be found.",
					"Check if the "
							+ ANDROID_HOME
							+ " environment variabe is pointing to the right location.");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}

		return dir;

	}

	public static File tools_home() {
		return get_directory("tools");
	}

	public static File platform_tools_home() {
		return get_directory("platform-tools");
	}

	public static File get_build_tools_home() {
		return get_directory("build-tools");
	}

	public static File adb() {
		File file = new File(platform_tools_home(), "adb"
				+ OS.get_platform_executable_suffix_exe());
		check_tool_existance(file);
		return file;

	}

	public static File android() {
		File file = new File(tools_home(), "android"
				+ OS.get_platform_executable_suffix_bat());
		check_tool_existance(file);
		return file;
	}

	public static File emulator() {
		File file = new File(tools_home(), "emulator"
				+ OS.get_platform_executable_suffix_exe());
		check_tool_existance(file);
		return file;
	}

	private static void check_tool_existance(File file) {
		if (file == null || !file.exists() || !file.isFile()) {
			MobilityException mex = new MobilityException(
					"Locating the " + file.getAbsolutePath() + "file",
					"The file " + file.getAbsolutePath()
							+ " could not be found.",
					"Check if the "
							+ ANDROID_HOME
							+ " environment variabe is pointing to the right location.");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}
	}
}
