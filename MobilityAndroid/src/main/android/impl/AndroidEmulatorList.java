package main.android.impl;

public class AndroidEmulatorList {

	public static final String Nexus_7_Gen_2_Jellybean_4_3 = "Nexus.7.Gen.2.Jellybean.4.3";
	public static final String Nexus_7_Gen_1_Jellybean_4_3 = "Nexus.7.Gen.1.Jellybean.4.3";
	public static final String Nexus_7_Gen_1_ICS_4_0_3 = "Nexus.7.Gen.1.ICS.4.0.3";
	public static final String Nexus_4_Jellybean_4_3 = "Nexus.4.Jellybean.4.3";
	public static final String Nexus_4_ICS_4_0_3 = "Nexus.4.ICS.4.0.3";
	public static final String Nexus_10_Jellybean_4_3 = "Nexus.10.Jellybean.4.3";
	public static final String Galaxy_S2_Jellybean_4_3 = "Galaxy.S2.Jellybean.4.3";
	public static final String Galaxy_S2_ICS_4_0_3 = "Galaxy.S2.ICS.4.0.3";
	public static final String Galaxy_S3_Jellybean_4_3 = "Galaxy.S3.Jellybean.4.3";
	public static final String Galaxy_S3_ICS_4_0_3 = "Galaxy.S3.ICS.4.0.3";
	public static final String Galaxy_S4_Jellybean_4_3 = "Galaxy.S4.Jellybean.4.3";
	public static final String Galaxy_Note_II_Jellybean_4_3 = "Galaxy.Note.II.Jellybean.4.3";
	public static final String Galaxy_Note_Jellybean_4_3 = "Galaxy.Note.Jellybean.4.3";
	public static final String Galaxy_Note_ICS_4_0_3 = "Galaxy.Note.ICS.4.0.3";

}
