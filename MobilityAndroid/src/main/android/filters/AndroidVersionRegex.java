package main.android.filters;

import java.io.File;
import java.io.FileFilter;

public class AndroidVersionRegex implements FileFilter {

	public boolean accept(File pathName) {

		String fileName = pathName.getName();
		String regex = "\\d{1}\\.\\d{1}\\.\\d{1}";
		String prefix = "android-";

		if (fileName.matches(regex) || fileName.startsWith(prefix)) {
			return true;
		}

		return false;

	}

}
