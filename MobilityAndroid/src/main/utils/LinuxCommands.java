package main.utils;

import java.lang.reflect.Field;
import java.util.logging.Level;

import main.exceptions.unchecked.MobilityException;
import main.exec.CommandExecutor;

import org.apache.commons.exec.CommandLine;

public class LinuxCommands {

	private static CommandLine whoami() {
		CommandLine command = new CommandLine("whoami");
		return command;
	}

	private static CommandLine kill(String severity, int pid) {
		CommandLine command = new CommandLine("kill");
		command.addArgument(severity);
		command.addArgument(Integer.toString(pid));
		return command;
	}

	public static int get_pid(Process process) {
		Field f;
		try {
			f = process.getClass().getDeclaredField("pid");
			f.setAccessible(true);
			return f.getInt(process);
		} catch (Exception e) {
			MobilityException mex = new MobilityException(
					"Extracting the Process ID (PID) for the desired process",
					"Could not determine the PID for the desired process",
					"Check the code for mistakes and look at the stack trace for further information about the problem",
					e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}
	}

	public static String kill_process(int pid) {
		String response = CommandExecutor.exec(kill("-9", pid));
		return response.trim();
	}

	public static String get_user() {
		String response = CommandExecutor.exec(whoami());
		return response.trim();
	}

	public static String get_home_dir(){
		String home = System.getenv("HOME");
		return home.trim();
	}
	
}
