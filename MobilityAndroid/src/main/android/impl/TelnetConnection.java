package main.android.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class TelnetConnection {

	private static final String LOCALHOST = "localhost";

	public static String send_command(String command, int port) throws IOException{
		return send_command(command, LOCALHOST, port);
	}

	private static String send_command(String command, String ipaddress, int port) throws IOException {

		Socket socket = null;
		BufferedReader in = null;
		PrintWriter out = null;
		String result = "";

		try {
			socket = new Socket(ipaddress, port);
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());

			out.write(command);
			out.write("\r\n");
			out.flush();

			in.readLine();
			in.readLine();
			result = in.readLine();

		} catch (UnknownHostException e) {
			throw new RuntimeException(
					"Error - Unknown Host specified for this telnet connection: "
							+ ipaddress);
		} finally {
			close_connection(socket, in, out);
		}
		return result;
	}

	private static void close_connection(Socket socket, BufferedReader in, PrintWriter out){
		try{
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
			if (socket != null) {
				socket.close();
			}
		}catch(IOException e){
			// TODO Log this error, do not swallow it but ignore it
		}
	}
	
}
