package main.android.impl;

import java.io.File;
import java.util.logging.Level;

import org.apache.commons.exec.CommandLine;

import main.android.interfaces.AndroidEmulator;
import main.exceptions.checked.EmulatorException;
import main.exceptions.unchecked.MobilityException;
import main.exec.CommandExecutor;
import main.utils.AndroidSdk;
import main.utils.Ant;
import main.utils.LinuxCommands;
import main.utils.Log;
import main.utils.SleepTimeout;

public class Emulator implements AndroidEmulator {

	public final String ANDROID_EMULATOR_HARDWARE_CONFIG = "hardware-qemu.ini";
	public final String FILE_LOCKING_SUFIX = ".lock";
	private final String EMULATOR_PREFIX = "emulator-";
	private final String LOG_LOCATION = "/data/data/com.cisco.webex.meetings/files";
	private final String LOG_DESTINATION = "reports-mobility";

	private String name;
	private String serial;
	private String target;
	private String abi;
	private File avdFolder;
	private String sdCard;
	private int portNumber;
	private int PID;
	private Process emulatorProcess;

	protected Emulator(Emulator emulator) {
		this(emulator.get_name(), emulator.get_target(), emulator.get_abi(),
				emulator.get_avd_folder(), emulator.get_sdcard());
	}

	protected Emulator(String name, String target, String abi,
			File avdFilePath, String sdCard) {
		this.name = name;
		this.target = target;
		this.abi = abi;
		this.avdFolder = avdFilePath;
		this.sdCard = sdCard;
		emulatorProcess = null;
	}

	public boolean is_started() {
		File lockedEmulatorHardwareConfig = new File(this.get_root_folder(),
				ANDROID_EMULATOR_HARDWARE_CONFIG + FILE_LOCKING_SUFIX);
		return lockedEmulatorHardwareConfig.exists();
	}

	public void start() throws EmulatorException {

		if (is_started()) {
			throw new EmulatorException(
					"Starting the specified emulator.",
					"Emulator is already started.",
					"Wait for current emulator to stop, or kill the running emulator with the same name.");
		}

		execute_start_command();

		wait_to_boot();

		unlock_screen();

	}

	private void execute_start_command() {
		CommandLine command = new CommandLine("emulator");
		command.addArgument("-port", false);
		command.addArgument((Integer.toString(get_port_number())), false);
		command.addArgument("-scale", false);
		command.addArgument("0.3", false);
		command.addArgument("@" + get_name(), false);
		emulatorProcess = CommandExecutor.exec_async(command);
		PID = LinuxCommands.get_pid(emulatorProcess);
	}

	private void wait_to_boot() throws EmulatorException {
		wait_to_boot(360);
	}

	private void wait_to_boot(long timeOutInSec) throws EmulatorException {

		try {
			Log.get_logger().log(Level.INFO, "[+] Starting the emulator");
			long startTime = System.nanoTime();
			while (!is_ready()) {
				double timeElapsed = ((System.nanoTime() - startTime) / 1000000000.0);
				if (timeElapsed < timeOutInSec) {
					Log.get_logger().log(
							Level.INFO,
							"[+] Checking if emulator: { " + serial
									+ " } booted... Check point at "
									+ timeElapsed + ". second.");
					Thread.sleep(SleepTimeout.MEDIUM);
				} else {
					if (emulatorProcess.isAlive()) {
						stop();
						throw new EmulatorException(
								"Starting the desired emulator.",
								"Emulator did not start in the specified time.",
								"Define a larger timeout");

					}
				}
			}
			Log.get_logger().log(Level.INFO,
					"[+] Giving the emulator some time to settle");
			Thread.sleep(SleepTimeout.MEDIUM * 2);
			Log.get_logger().log(
					Level.INFO,
					"[+] Emulator {" + serial + "} is ready for use...\nPID: "
							+ PID);
		} catch (InterruptedException e) {
			MobilityException mex = new MobilityException(
					"Waiting the emulator to boot",
					"The sleep Thread was interupted",
					"There is really nothing you can do about this.", e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;

		}
	}

	private boolean is_ready() {

		try {
			CommandLine command = adb_command();
			command.addArgument("shell", false);
			command.addArgument("getprop", false);
			command.addArgument("init.svc.bootanim", false);

			String bootAnimation = CommandExecutor.exec(command);

			if (bootAnimation != null && bootAnimation.contains("stopped")) {
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	private int get_port_number() {
		portNumber = EmulatorControlCenter.get_instance()
				.available_port_number();
		serial = EMULATOR_PREFIX + portNumber;
		return portNumber;
	}

	public void stop() {
		try {
			TelnetConnection.send_command("kill", get_port());
			Log.get_logger().log(Level.INFO, "[+] Killed with telnet...");
			Thread.sleep(SleepTimeout.SHORT);
		} catch (Exception e) {
			CommandLine command = adb_command();
			command.addArgument("emu");
			command.addArgument("kill");
			CommandExecutor.exec(command);
			Log.get_logger().log(Level.INFO, "[+] Killed with adb...");
		} finally {
			if (is_running() || is_started()) {
				LinuxCommands.kill_process(PID);
				Log.get_logger().log(Level.INFO,
						"[+] Killed forcibly through OS shell...");
			}
		}
	}

	public void delete() {
		CommandLine command = new CommandLine(AndroidSdk.android());
		command.addArgument("delete", false);
		command.addArgument("avd", false);
		command.addArgument("-n", false);
		command.addArgument(get_name(), false);

		CommandExecutor.exec(command);
	}

	public void unlock_screen() {
		CommandLine event82 = adb_command();

		event82.addArgument("shell", false);
		event82.addArgument("input", false);
		event82.addArgument("keyevent", false);
		event82.addArgument("82", false);

		CommandExecutor.exec(event82, 20000);

		CommandLine event4 = adb_command();

		event4.addArgument("shell", false);
		event4.addArgument("input", false);
		event4.addArgument("keyevent", false);
		event4.addArgument("4", false);

		CommandExecutor.exec(event4, 20000);

	}

	@Override
	public boolean equals(Object arg0) {
		try {
			Emulator emulator = (Emulator) arg0;
			if (emulator.get_serial().equals(this.get_serial())) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuffer data = new StringBuffer();
		data.append("[+] Printing emulator data\n");

		String nameData = get_name() != null ? "Name: " + get_name() + "\n"
				: "";
		data.append(nameData);

		String targetData = get_target() != null ? "Target: " + get_target()
				+ "\n" : "";
		data.append(targetData);

		String pathData = get_avd_folder() != null ? "Path: "
				+ get_avd_folder() + "\n" : "";
		data.append(pathData);

		String abiData = get_abi() != null ? "ABI: " + get_abi() + "\n" : "";
		data.append(abiData);

		String sdcardData = get_sdcard() != null ? "Sdcard: " + get_sdcard()
				+ "\n" : "";
		data.append(sdcardData);

		return data.toString();
	}

	public void install_apk(File apk) {

		Log.get_logger().log(Level.INFO, "[+] Installing APK");

		CommandLine command = adb_command();
		command.addArgument("install", false);
		command.addArgument("-r", false);
		command.addArgument(apk.getAbsolutePath(), false);

		String out = CommandExecutor.exec(command, 1200000);

		Log.get_logger().log(Level.INFO, out);

		try {
			Thread.sleep(SleepTimeout.MEDIUM);
		} catch (InterruptedException e) {
			MobilityException mex = new MobilityException("Installing apk",
					"The sleep Thread was interupted",
					"There is really nothing you can do about this.", e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}

		Log.get_logger().log(Level.INFO, "[+] Installation successful...");

	}

	public void install_test_buildxm(File test) {

		Log.get_logger().log(Level.INFO, "[+] Installing TEST");

		CommandLine command = new CommandLine(Ant.ant());
		command.addArgument("-buildfile", false);
		command.addArgument(test.getAbsolutePath(), false);
		command.addArgument("clean", false);
		command.addArgument("debug", false);
		command.addArgument("install", false);

		String out = CommandExecutor.exec(command, 3600000);

		Log.get_logger().log(Level.INFO, out);

		try {
			Thread.sleep(SleepTimeout.MEDIUM);
		} catch (InterruptedException e) {
			MobilityException mex = new MobilityException(
					"Installing test apk", "The sleep Thread was interupted",
					"There is really nothing you can do about this.", e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}

		Log.get_logger().log(Level.INFO, "[+] Installation successful...");

	}

	public void install_test_and_run_test_buildxml(File test) {

		Log.get_logger().log(Level.INFO, "[+] Installing and Running TESTS");

		CommandLine command = new CommandLine(Ant.ant());
		command.addArgument("-buildfile", false);
		command.addArgument(test.getAbsolutePath(), false);
		command.addArgument("clean", false);
		command.addArgument("debug", false);
		command.addArgument("install", false);
		command.addArgument("test", false);

		String out = CommandExecutor.exec(command, 3600000);

		Log.get_logger().log(Level.INFO, out);

		try {
			Thread.sleep(SleepTimeout.MEDIUM);
		} catch (InterruptedException e) {
			MobilityException mex = new MobilityException(
					"Installing test apk", "The sleep Thread was interupted",
					"There is really nothing you can do about this.", e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}

		Log.get_logger().log(Level.INFO, "[+] Run successful...");

	}

	public boolean is_installed(String apkPath) {
		// TODO Auto-generated method stub
		return false;
	}

	public void uninstall(String apkPath) {
		// TODO Auto-generated method stub
	}

	public void run_test_instrumentation(String testName) {

		Log.get_logger().log(Level.INFO, "[+] Running TEST");

		try {
			CommandLine command = adb_command();
			command.addArgument("shell", false);
			command.addArgument("am", false);
			command.addArgument("instrument", false);
			command.addArgument("-w", false);
			command.addArgument("-e", false);
			command.addArgument("class", false);
			command.addArgument(testName, false);
			command.addArgument(
					"com.test/android.test.InstrumentationTestRunner", false);

			String out = CommandExecutor.exec(command, 1200000);

			Log.get_logger().log(Level.INFO, out);

		} catch (Exception e) {
			Log.get_logger()
					.log(Level.INFO,
							"[-] Command is either incorrect or it took too long to execute");
		}

		try {
			Thread.sleep(SleepTimeout.MEDIUM);
		} catch (InterruptedException e) {
			MobilityException mex = new MobilityException("Running tests",
					"The sleep Thread was interupted",
					"There is really nothing you can do about this.", e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}

		Log.get_logger().log(Level.INFO, "[+] Test executed successfully...");

	}

	public void pull_logs() {

		Log.get_logger().log(Level.INFO, "[+] Pulling Loggs");

		CommandLine command = adb_command();
		command.addArgument("pull", false);
		command.addArgument(LOG_LOCATION, false);
		command.addArgument(LOG_DESTINATION, false);

		String out = CommandExecutor.exec(command, 300000);

		Log.get_logger().log(Level.INFO, out);

		try {
			Thread.sleep(SleepTimeout.MEDIUM);
		} catch (InterruptedException e) {
			MobilityException mex = new MobilityException("Pulling logs",
					"The sleep Thread was interupted",
					"There is really nothing you can do about this.", e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}

		Log.get_logger().log(Level.INFO, "[+] Logs pulled successfully...");

	}

	public int get_port() {
		return portNumber;
	}

	public String get_serial() {
		return serial;
	}

	private boolean is_running() {
		return emulatorProcess.isAlive();
	}

	public File get_root_folder() {
		return avdFolder;
	}

	public String get_name() {
		return name;
	}

	private String get_target() {
		return target;
	}

	private String get_abi() {
		return abi;
	}

	private File get_avd_folder() {
		return avdFolder;
	}

	private String get_sdcard() {
		return sdCard;
	}

	private CommandLine adb_command() {
		CommandLine command = new CommandLine(AndroidSdk.adb());
		command.addArgument("-s", false);
		command.addArgument(serial, false);
		return command;
	}

}
