package test;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import main.android.impl.AndroidEmulatorList;
import main.android.impl.EmulatorControlCenter;
import main.android.interfaces.AndroidEmulator;
import main.exceptions.checked.EmulatorException;
import main.exceptions.unchecked.MobilityException;
import main.utils.Log;

public class MainTest {

	public static void main(String[] args) throws EmulatorException,
			UnknownHostException, IOException, InterruptedException {

		// Start the logger
		Log.init();

		// Check parameters
		List<File> files = are_parameters_ok(args);

		if (files == null || files.isEmpty() || files.size() != 2) {
			MobilityException mex = new MobilityException(
					"Checking parameters",
					"Bad parameters",
					"Check the parameters. You must provide exactly two parameters first being PATH_TO_WEBEX_APK and second being PATH_TO_TEST_BUILD_XML");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}

		// Create and configure Emulators
		EmulatorControlCenter.get_instance().configure_emulators();

		// Start the wanted emulator
		AndroidEmulator Galaxy_S4 = EmulatorControlCenter.get_instance()
				.start_emulator(AndroidEmulatorList.Galaxy_S4_Jellybean_4_3);

		// Install .apk
		Galaxy_S4.install_apk(files.get(0));

		// Install Test .apk
		Galaxy_S4.install_test(files.get(1));

		// Run tests
		Galaxy_S4.run_test("tests.AllTests");

		// Install Test and run Suite
		Galaxy_S4.install_test_and_run_test_buildxml(files.get(1));

		// Collect report(s)
		Galaxy_S4.pull_logs();

		// Stop the Emulator
		Galaxy_S4.stop();

	}

	public static List<File> are_parameters_ok(String[] args) {

		Log.get_logger().log(Level.INFO, "[+] Are parameters ok ???");

		List<File> files = new ArrayList<File>();

		try {

			if (args.length != 2) {
				return null;
			}

			File apk = new File(args[0]);
			File buildxml = new File(args[1]);

			if (!apk.exists() || !buildxml.exists()) {
				return null;
			}

			String apkType = Files.probeContentType(apk.toPath());
			String buildxmlType = Files.probeContentType(buildxml.toPath());

			if (!apkType.equals("application/zip")
					|| !buildxmlType.equals("application/xml")) {
				return null;
			}

			files.add(apk);
			files.add(buildxml);

		} catch (IOException e) {
			return null;
		}

		Log.get_logger().log(Level.INFO, "[+] Parameters are ok !!!");
		return files;
	}

}
