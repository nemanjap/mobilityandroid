package main.utils;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Log {

	private final static Logger logger = Logger.getLogger(Log.class.getName());
	private static FileHandler fh = null;

	public static Logger get_logger(){
		return logger;
	}
	
	public static void init() {
		try {
			fh = new FileHandler("mobility.log", false);
			Logger l = Logger.getLogger("");
			fh.setFormatter(new SimpleFormatter());
			l.addHandler(fh);
			l.setLevel(Level.FINE);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
