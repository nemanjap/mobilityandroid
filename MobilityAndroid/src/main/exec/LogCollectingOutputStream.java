package main.exec;

import org.apache.commons.exec.LogOutputStream;

public class LogCollectingOutputStream extends LogOutputStream {

	private StringBuilder output = new StringBuilder();

	@Override
	protected void processLine(String line, int level) {
		output.append(line).append("\n");
	}

	public String getOutput() {
		return output.toString();
	}

}
