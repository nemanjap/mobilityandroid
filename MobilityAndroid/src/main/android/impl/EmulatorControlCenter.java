package main.android.impl;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.yaml.snakeyaml.Yaml;

import main.android.filters.AvdFilter;
import main.android.interfaces.AndroidEmulator;
import main.exceptions.checked.EmulatorException;
import main.exceptions.unchecked.MobilityException;
import main.exec.CommandExecutor;
import main.utils.AndroidSdk;
import main.utils.Log;
import main.utils.PatternMatcher;

public class EmulatorControlCenter {

	private final String EMULATOR_PREFIX = "emulator-";
	private String AVD_SUFIX = ".avd";

	private static EmulatorControlCenter instance = null;

	private EmulatorControlCenter() {

	}

	public static synchronized EmulatorControlCenter get_instance() {
		if (instance == null) {
			instance = new EmulatorControlCenter();
		}
		return instance;
	}

	public boolean does_emulator_exist(String emulatorName) {
		File emulatorFolder = new File(AndroidSdk.avd_home(), emulatorName
				+ ".avd");

		return emulatorFolder.exists();
	}

	public boolean is_emulator_started(Emulator emulator) {
		return emulator.is_started();
	}

	public List<String> available_emulators() throws EmulatorException {

		File[] emulatorFiles = AndroidSdk.avd_home().listFiles(new AvdFilter());

		if (emulatorFiles == null || emulatorFiles.length == 0) {
			throw new EmulatorException(
					"Listing all available emulators on system, as specifid with ANDROID_SDK_HOME variable.",
					"There are no created emulators on the system.",
					"Check if the ANDROID_SDK_HOME is pointing to the right directory, and create a emulator if none are created.");
		}

		List<String> emulators = new ArrayList<String>();

		for (File emulator : emulatorFiles) {
			String emulatorName = PatternMatcher.get_data("^(.*?)\\"
					+ AVD_SUFIX + "$", emulator.getName(), 1);
			emulators.add(emulatorName);
		}

		return emulators;

	}

	public AndroidEmulator get_emulator(String emulatorName)
			throws EmulatorException {

		AndroidEmulator emulator = null;

		CommandLine cmd = new CommandLine(AndroidSdk.android());
		cmd.addArgument("list", false);
		cmd.addArgument("avds", false);

		String output = null;
		output = CommandExecutor.exec(cmd);

		String[] avds = output.split("---------");

		if (avds != null && avds.length > 0) {
			for (String avdData : avds) {
				if (!avdData.contains("Name:")) {
					continue;
				}
				String name = avd_name(avdData);
				if (name.equals(emulatorName)) {
					File avdFilePath = avd_path(avdData);
					String target = avd_target(avdData);
					String abi = avd_abi(avdData);
					String sdCard = sdcard(avdData);

					emulator = new Emulator(name, target, abi, avdFilePath,
							sdCard);
					return emulator;
				}
			}
		}
		throw new EmulatorException(
				"Trying to get the information aboud a emulator with the specified name.",
				"There is no such emulator in the default avd folder.",
				"Create the emulator through Eclipse or by using the avd create command.");
	}

	public int available_port_number() {
		List<Integer> allPorts = ports_all();
		allPorts.removeAll(ports_in_use());

		return allPorts.get(0);
	}

	public List<String> running_emulators() {

		List<String> emulators = new ArrayList<String>();

		CommandLine cmd = new CommandLine(AndroidSdk.adb());
		cmd.addArgument("devices", false);

		String output = CommandExecutor.exec(cmd);

		Scanner scanner = new Scanner(output);
		while (scanner.hasNextLine()) {

			String line = scanner.nextLine();

			if (line.contains(EMULATOR_PREFIX)) {
				String emulatorData = PatternMatcher.get_data(
						"emulator-(.*)\\d", line, 0);
				emulators.add(emulatorData);
			}

		}
		scanner.close();
		return emulators;
	}

	public List<Integer> ports_in_use() {
		List<Integer> ports = new ArrayList<Integer>();
		List<String> emulators = running_emulators();

		for (String emulatorData : emulators) {
			ports.add(Integer.parseInt(emulatorData.replaceAll(EMULATOR_PREFIX,
					"")));
		}

		return ports;
	}

	public AndroidEmulator start_emulator(String emulatorName)
			throws EmulatorException {
		AndroidEmulator emulator = get_emulator(emulatorName);
		emulator.start();
		return emulator;
	}

	public void configure_emulators() {

		Log.get_logger().log(Level.INFO,
				"[+] Configuring emulators...");

		InputStream yamlFile;
		try {
			yamlFile = new FileInputStream(new File("emulators"));
		} catch (FileNotFoundException e1) {
			MobilityException mex = new MobilityException(
					"Parsing the YAML file",
					"The specified file - emulators could not be found",
					"Create the emulators YAML file", e1);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}

		Yaml yaml = new Yaml();

		@SuppressWarnings("unchecked")
		ArrayList<LinkedHashMap<String, Object>> results = (ArrayList<LinkedHashMap<String, Object>>) yaml
				.load(yamlFile);

		for (LinkedHashMap<String, Object> emulator : results) {
			Executor exec = new DefaultExecutor();

			String[] args = { "create", "avd", "--force", "--abi",
					(String) emulator.get("abi"), "--skin",
					(String) emulator.get("skin"), "--target",
					(String) emulator.get("target"), "--name",
					(String) emulator.get("name") };

			CommandLine command = new CommandLine(AndroidSdk.android());
			command.addArguments(args);

			String text = "no";

			ByteArrayInputStream input;
			try {
				input = new ByteArrayInputStream(text.getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e) {
				MobilityException mex = new MobilityException(
						"Setting the input encoding",
						"Encoding specified is not supported",
						"Change the input encoding", e);
				Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
						mex.fillInStackTrace());
				throw mex;
			}
			ByteArrayOutputStream output = new ByteArrayOutputStream();

			exec.setStreamHandler(new PumpStreamHandler(output, null, input));
			try {
				exec.execute(command);
			} catch (ExecuteException e) {
				MobilityException mex = new MobilityException(
						"Executing avd create command", "Unknown",
						"Check the command syntax for errors", e);
				Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
						mex.fillInStackTrace());
				throw mex;
			} catch (IOException e) {
				MobilityException mex = new MobilityException(
						"IO exception while executing avd create command",
						"Unknown", "Unknown", e);
				Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
						mex.fillInStackTrace());
				throw mex;
			}

			update_emulator_config(emulator, get_avd_config_options());

		}

		Log.get_logger().log(Level.INFO,
				"[+] Configuration complete");
	}

	private StringBuilder get_avd_config_options() {
		StringBuilder configOptions = new StringBuilder();
		configOptions.append("\ndisk.dataPartition.size=200M");
		configOptions.append("\nhw.accelerometer=yes");
		configOptions.append("\nhw.audioInput=yes");
		configOptions.append("\nhw.battery=yes");
		configOptions.append("\nhw.camera.back=none");
		configOptions.append("\nhw.camera.front=none");
		configOptions.append("\nhw.dPad=no");
		configOptions.append("\nhw.gps=yes");
		configOptions.append("\nhw.keyboard=yes");
		configOptions.append("\nhw.mainKeys=no");
		configOptions.append("\nhw.ramSize=512");
		configOptions.append("\nhw.sdCard=no");
		configOptions.append("\nhw.sensors.orientation=yes");
		configOptions.append("\nhw.sensors.proximity=yes");
		configOptions.append("\nhw.trackBall=no");
		configOptions.append("\nskin.dynamic=no");
		configOptions.append("\nvm.heapSize=64");

		return configOptions;
	}

	private void update_emulator_config(LinkedHashMap<String, Object> emulator,
			StringBuilder configOptions) {

		StringBuffer path = new StringBuffer();
		path.append(AndroidSdk.avd_home());
		path.append(File.separator);
		path.append(emulator.get("name").toString() + ".avd");
		path.append(File.separator);
		path.append("config.ini");
		path.append(File.separator);

		File configFile = new File(path.toString());

		Log.get_logger().log(Level.INFO,
				"[+] Checking if:" + configFile.getPath() + " exists.");

		if (!configFile.exists()) {
			MobilityException mex = new MobilityException(
					"Configuring emulators",
					"Configuration ini file for the emulator does not exist",
					"Check the path of the file that you are trying to see if exists, and check the avd create command for errors.");
			Log.get_logger().log(Level.SEVERE, mex.getMessage());
			throw mex;
		}

		try {
			FileWriter fileWriter = new FileWriter(configFile, true);
			BufferedWriter bufferWriter = new BufferedWriter(fileWriter);

			LinkedHashMap<String, Object> configuration = (LinkedHashMap<String, Object>) emulator
					.get("config");

			bufferWriter.write(configOptions.toString());

			for (Map.Entry<String, Object> entry : configuration.entrySet()) {
				bufferWriter.write("\n" + entry.getKey() + "="
						+ entry.getValue());
			}
			bufferWriter.close();
			Log.get_logger().log(Level.INFO, "[+] Data is written..");
		} catch (IOException e) {
			MobilityException mex = new MobilityException(
					"Adding config options to emulators config file",
					"IOException", "Check the stack trace", e);
			Log.get_logger().log(Level.SEVERE, mex.getCause().toString(),
					mex.fillInStackTrace());
			throw mex;
		}

	}

	private String avd_name(String avdData) {
		return PatternMatcher.get_data("Name\\: (.+)", avdData, 1);
	}

	private File avd_path(String avdData) {
		return new File(PatternMatcher.get_data("Path\\: (.+)", avdData, 1));
	}

	private String avd_target(String avdData) {
		return PatternMatcher.get_data("Target:(.*)\\(", avdData, 1);
	}

	private String avd_abi(String avdData) {
		return PatternMatcher.get_data("ABI\\: (.+)", avdData, 1);
	}

	private String sdcard(String avdData) {
		return PatternMatcher.get_data("Sdcard\\: (.+)", avdData, 1);
	}

	private List<Integer> ports_all() {
		List<Integer> allPorts = new ArrayList<Integer>();
		for (int i = 5554; i < 5585; i = i + 2) {
			allPorts.add(i);
		}
		return allPorts;
	}

}
